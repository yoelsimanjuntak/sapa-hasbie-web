<?php
$ruser = GetLoggedUser();
$rpengguna = $this->db->where(COL_ROLEID, ROLEPUBLIC)->count_all_results(TBL__USERS);
$rlaporan = $this->db->where(COL_LAPKATEGORI, 'LAPORAN')->count_all_results(TBL_TLAPORAN);
$raduan = $this->db->where(COL_LAPKATEGORI, 'LAPORAN')->count_all_results(TBL_TLAPORAN);
$rpesan = $this->db
->where('from.RoleID', ROLEPUBLIC)
->where("DATE(Timestamp)", date('Y-m-d'))
->join(TBL__USERS.' from','from.'.COL_USERNAME." = ".TBL_TPESAN.".".COL_PESANDARI,"left")
->count_all_results(TBL_TPESAN);
 ?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-3 col-6">
        <div class="small-box bg-olive">
          <div class="inner">
            <h3><?=number_format($rpengguna)?></h3>

            <p class="font-italic">PENGGUNA</p>
          </div>
          <div class="icon">
            <i class="fas fa-users"></i>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-6">
        <div class="small-box bg-gray disabled">
          <div class="inner">
            <h3><?=number_format($rlaporan)?></h3>

            <p class="font-italic">LAPORAN</p>
          </div>
          <div class="icon">
            <i class="fas fa-envelope-open-text"></i>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-6">
        <div class="small-box bg-olive">
          <div class="inner">
            <h3><?=number_format($raduan)?></h3>

            <p class="font-italic">ADUAN</p>
          </div>
          <div class="icon">
            <i class="fas fa-comment-alt-exclamation"></i>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-6">
        <div class="small-box bg-gray disabled">
          <div class="inner">
            <h3 data-label="chat-count"><?=number_format($rpesan)?></h3>

            <p class="font-italic">PESAN <span class="text-sm"><?=date('d-m-Y')?></span></p>
          </div>
          <div class="icon">
            <i class="fas fa-comments-alt"></i>
          </div>
        </div>
      </div>
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEMANAGER) {
        ?>
        <div class="col-sm-12">
          <div class="card card-outline card-olive">
            <div class="card-header">
              <h5 class="card-title">LIVE CHAT</h5>
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i></button>
              </div>
            </div>
            <div class="card-body">
              <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover table-condensed" style="margin-top: 0 !important">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 10px">OPSI</th>
                      <th>TANGGAL / WAKTU</th>
                      <th>EMAIL / NO. HP</th>
                      <th>NAMA</th>
                      <th>ISI</th>
                      <th>BELUM DIBACA</th>
                      <th>JLH. PESAN</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </form>
            </div>
          </div>
        </div>
        <?php
      }
      ?>

    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  <?php
  if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEMANAGER) {
    ?>
    var dt = $('#datalist').dataTable({
      "autoWidth" : false,
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?=site_url('site/chat/index-load')?>",
        "type": 'POST',
        "data": function(data){
          //data.filterChatStatus = $('[name=filterChatStatus]', $('.filtering')).val();
         }
      },
      "scrollY" : '32vh',
      "scrollX": "200%",
      "iDisplayLength": 100,
      "oLanguage": {
        "sSearch": "FILTER "
      },
      "dom":"R<'row'><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
      "ordering": false,
      "columnDefs": [
        {"targets":[0,5,6], "className":'nowrap text-center'},
        {"targets":[1], "className":'nowrap dt-body-right'},
        {"targets":[2], "className":'nowrap'}
      ],
      "columns": [
        {"orderable": false,"width": "10px"},
        {"orderable": false,"width": "50px"},
        {"orderable": false},
        {"orderable": false},
        {"orderable": false},
        {"orderable": false,"width": "50px"},
        {"orderable": false,"width": "50px"}
      ],
      "createdRow": function(row, data, dataIndex) {
        $('.btn-chat-open', $(row)).click(function() {
          var url = $(this).attr('href');
          var window_ = window.open(url, "LIVE_CHAT", "left=100,top=100,width=600,height=800");
          return false;
        });
        $('[data-toggle="tooltip"]', $(row)).tooltip();
      },
      "initComplete": function(settings, json) {
        $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
      }
    });

    $('.btn-refresh-data').click(function() {
      dt.DataTable().ajax.reload();
    });
    $('input,select', $("div.filtering")).change(function() {
      dt.DataTable().ajax.reload();
    });
    <?php
  }
  ?>

  setInterval(function(){
    dt.DataTable().ajax.reload();
    $.get("<?=site_url('site/api/message-count')?>", function(res, status){
      res = JSON.parse(res);
      $('[data-label=chat-count]').html(res.data?res.data:'0');
    });
  }, 5000);
});
</script>
