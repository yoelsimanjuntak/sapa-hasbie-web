<?php $data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_USERNAME] . '" />',
        $d[COL_ISSUSPEND] ? '<small class="badge pull-left badge-danger">Suspend</small>' : '<small class="badge pull-left badge-success">Active</small>',
        anchor('user/edit/'.$d[COL_USERNAME],$d[COL_USERNAME]),
        $d[COL_ROLENAME],
        $d[COL_EMAIL],
        $d[COL_NAME],
        //substr($d[COL_COMPANYADDRESS], 0, 25),
        (!empty($d[COL_LASTLOGIN])?date('d M Y H:i:s', strtotime($d[COL_LASTLOGIN])):"-")
    );
    $i++;
}
$data = json_encode($res);
?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?> <small class="text-sm">Data</small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
                    <li class="breadcrumb-item active">Pengguna</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <p>
                    <?=anchor('user/delete','<i class="fas fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
                    <?=anchor('user/activate','<i class="fas fa-check"></i> Aktifkan',array('class'=>'cekboxaction btn btn-success btn-sm','confirm'=>'Apa anda yakin?'))?>
                    <?=anchor('user/activate/1','<i class="fas fa-warning"></i> Suspend',array('class'=>'cekboxaction btn btn-warning btn-sm','confirm'=>'Apa anda yakin?'))?>
                    <?=anchor('user/add','<i class="fas fa-plus"></i> Data Baru',array('class'=>'btn btn-primary btn-sm'))?>
                </p>
                <div class="card card-default">
                    <div class="card-body">
                        <form id="dataform" method="post" action="#">
                            <table id="datalist" class="table table-bordered table-hover" style="white-space: nowrap;">

                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
              "autoWidth" : false,
              //"sDom": "Rlfrtip",
              "aaData": <?=$data?>,
              //"bJQueryUI": true,
              //"aaSorting" : [[5,'desc']],
              "scrollY" : '40vh',
              "scrollX": "200%",
              "iDisplayLength": 100,
              "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
              "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
              "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
              "order": [[ 2, "asc" ]],
              "aoColumns": [
                  {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","width":"10px","bSortable":false},
                  {"sTitle": "Status","bSortable":false},
                  {"sTitle": "Username",},
                  {"sTitle": "Role"},
                  {"sTitle": "Email"},
                  {"sTitle": "Name"},
                  //{"sTitle": "Address"},
                  //{"sTitle": "Last Login", "width": "15%"}
              ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });
    </script>
