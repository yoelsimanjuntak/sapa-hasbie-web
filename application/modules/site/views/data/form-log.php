<form id="form-log" action="<?=current_url()?>" enctype="multipart/form-data">
  <div class="form-group">
    <label>CATATAN</label>
    <textarea class="form-control" rows="4" placeholder="Uraian catatan.." name="<?=COL_LOGREMARKS?>"></textarea>
  </div>

  <div class="form-group">
    <label>LAMPIRAN</label>
    <div id="div-attachment">
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
        </div>
        <div class="custom-file">
          <input type="file" class="custom-file-input" name="file" accept="image/*,application/pdf,application/vnd.ms-excel">
          <label class="custom-file-label" for="file">PILIH FILE</label>
        </div>
      </div>
      <p class="text-sm text-muted mb-0 font-italic">
        <strong>CATATAN:</strong><br />
        - Besar file / dokumen maksimum <strong>5 MB</strong><br />
        - Jenis file / dokumen yang diperbolehkan hanya dalam format <strong>JPG / JPEG</strong>, <strong>PNG</strong>, <strong>PDF</strong>, <strong>DOC / DOCX</strong>, <strong>XLS / XLSX</strong>
      </p>
    </div>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
  $('#form-log').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });

      return false;
    }
  });
});
</script>
