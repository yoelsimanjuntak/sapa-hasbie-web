<?php
$ruser = GetLoggedUser();
$rlog = $this->db
->select("tlaporanlog.*, COALESCE(_userinformation.Name, tlaporanlog.LogUserName) as LogName, '' as LogDescription, tlaporan.LapKategori")
->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TLAPORANLOG.".".COL_LOGUSERNAME,"left")
->join(TBL_TLAPORAN,TBL_TLAPORAN.'.'.COL_LAPID." = ".TBL_TLAPORANLOG.".".COL_LAPID,"left")
->where(TBL_TLAPORAN.'.'.COL_LAPID, $data[COL_LAPID])
->order_by(COL_LOGTIMESTAMP, 'desc')
->get(TBL_TLAPORANLOG)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=ucwords($data[COL_LAPKATEGORI]).' <strong>#'.str_pad($data[COL_LAPID],4,"0",STR_PAD_LEFT).'</strong>';?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <div class="card card-outline card-default">
          <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
          <div class="card-header">
            <h6 class="card-title">RINCIAN</h6>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label>JENIS</label>
                  <input type="text" class="form-control" name="<?=COL_LAPTIPE?>" value="<?=$data[COL_LAPTIPE]?>" disabled />
                </div>
              </div>
              <div class="col-sm-7">
                <div class="form-group">
                  <label>JUDUL</label>
                  <input type="text" class="form-control" name="<?=COL_LAPJUDUL?>" value="<?=$data[COL_LAPJUDUL]?>" disabled />
                </div>
              </div>
            </div>
            <?php
            if(!empty($data[COL_LAPTANGGAL])) {
              ?>
              <div class="row">
                <div class="col-sm-5">
                  <div class="form-group">
                    <label>TANGGAL</label>
                    <input type="text" class="form-control" name="<?=COL_LAPTANGGAL?>" value="<?=$data[COL_LAPTANGGAL]?>" disabled />
                  </div>
                </div>
              </div>
              <?php
            }
            ?>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>
                    KETERANGAN
                  </label>
                  <textarea class="form-control" name="<?=COL_LAPKETERANGAN?>" rows="5" disabled><?=$data[COL_LAPKETERANGAN]?></textarea>
                </div>
              </div>
            </div>
            <?php
            if(!empty($data[COL_LAPTANGGAL])) {
              ?>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>
                      LOKASI
                    </label>
                    <textarea class="form-control" name="<?=COL_LAPLOKASI?>" rows="3" disabled><?=$data[COL_LAPLOKASI]?></textarea>
                  </div>
                </div>
              </div>
              <?php
            }
            ?>
            <?php
            if(!empty($data[COL_LAPFILE])) {
              $files_ = explode(",",$data[COL_LAPFILE]);
              ?>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>LAMPIRAN</label>
                    <ul class="todo-list ui-sortable" data-widget="todo-list">
                      <?php
                      foreach($files_ as $f) {
                        ?>
                        <li>
                          <div class="d-inline ml-2">
                            <i class="far fa-paperclip"></i>
                          </div>
                          <span class="text font-italic"><?=$f?></span>
                          <div class="tools">
                            <a href="javascript:window.open('<?=MY_UPLOADURL.$f?>')"><i class="far fa-eye"></i></a>
                          </div>
                        </li>
                        <?php
                      }
                      ?>

                    </ul>
                  </div>
                </div>
              </div>
              <?php
            }
            ?>

            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label>STATUS</label>
                  <select class="form-control" name="<?=COL_LAPSTATUS?>" style="width: 100%" <?=$ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEMANAGER ? 'disabled' :''?>>
                    <?=GetCombobox("select * from mstatus order by StatusSeq", COL_STATUSNAME, COL_STATUSNAME, $data[COL_LAPSTATUS])?>
                  </select>
                </div>
              </div>
              <?php
              if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEMANAGER) {
                ?>
                <div class="col-sm-7">
                  <div class="form-group">
                    <label>PIC</label>
                    <select class="form-control" name="<?=COL_LAPPIC?>[]" multiple style="width: 100%">
                      <?=GetCombobox("select * from _userinformation ui left join _users u on u.UserName = ui.UserName where u.RoleID = ".ROLEOPERATOR." order by Name", COL_EMAIL, COL_NAME, explode(",", $data[COL_LAPPIC]))?>
                    </select>
                  </div>
                </div>
                <?php
              }
              ?>
            </div>
          </div>
          <div class="card-footer">
            <a href="<?=site_url('site/data/index/'.strtolower($data[COL_LAPKATEGORI]))?>" class="btn btn-secondary btn-sm"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
            <?php
            if($ruser[COL_ROLEID]==ROLEADMIN || $ruser[COL_ROLEID]==ROLEMANAGER) {
              ?>
              <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-check-circle"></i>&nbsp;PERBARUI</button>
              <?php
            }
            ?>

          </div>
          <?=form_close()?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-default">
          <div class="card-header">
            <h6 class="card-title">RIWAYAT / CATATAN</h6>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th style="width: 50px; white-space: nowrap">WAKTU</th>
                  <th>KETERANGAN</th>
                  <th style="width: 50px; white-space: nowrap">OLEH</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if(!empty($rlog)) {
                  foreach($rlog as $r) {
                    $desc_ = $r[COL_LOGREMARKS];
                    $files_ = "";
                    if($r[COL_LOGTIPE]=='STATUS') {
                      $desc_ = 'Status berubah menjadi <strong>'.$r[COL_LOGREMARKS].'</strong>.';
                      if($r[COL_LOGREMARKS]=='BARU') {
                        $desc_ = $r[COL_LAPKATEGORI].' <strong>'.str_pad($r[COL_LAPID],4,"0",STR_PAD_LEFT).'</strong> diterima.';
                      }
                    }

                    if(!empty($r[COL_LOGFILE])) {
                      $arrfile = explode(",", $r[COL_LOGFILE]);
                      foreach($arrfile as $f) {
                        if(file_exists(MY_UPLOADPATH.$f)) $files_ .= '<a href="'.(MY_UPLOADURL.$f).'" target="_blank"><i class="far fa-link pr-2"></i></a>';
                      }
                    }
                    ?>
                    <tr>
                      <td style="width: 50px; white-space: nowrap"><?=date('d-m-Y', strtotime($r[COL_LOGTIMESTAMP]))?>&nbsp;<sup class="font-italic"><?=date('H:i', strtotime($r[COL_LOGTIMESTAMP]))?></sup></td>
                      <td><?=$desc_?><?=!empty($r[COL_LOGFILE])?'<span class="pull-right">'.$files_.'</span>':''?></td>
                      <td style="max-width:250px; overflow:hidden !important; text-overflow:ellipsis; white-space:nowrap"><?=$r['LogName']?></td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="3" class="text-center font-italic text-sm">KOSONG</td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <div class="card-footer">
            <a id="btn-add-log" href="<?=site_url('site/data/form-log/'.$data[COL_LAPID])?>" class="btn btn-primary btn-sm"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH CATATAN</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalLog" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">TAMBAH CATATAN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer d-block">
        <div class="row">
          <div class="col-lg-12 text-center">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
            <button type="submit" class="btn btn-primary btn-submit"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalLog = $('#modalLog');
$(document).ready(function() {
  modalLog.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalLog).empty();
  });

  $('#form-main').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            } else {
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          }
        },
        error: function(data) {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('#btn-add-log').click(function(){
    var url = $(this).attr('href');
    modalLog.modal('show');
    $('.modal-body', modalLog).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modalLog).load(url, function(){
      $('button[type=submit]', modalLog).unbind('click').click(function(){
        $('form', modalLog).submit();
      });
    });
    return false;
  });
});
</script>
