<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, viewport-fit=cover">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <!-- Color theme for statusbar (Android only) -->
    <meta name="theme-color" content="#ff3b30">
    <!-- Your app title -->
    <title><?=$title?></title>
    <!-- Path to Framework7 Library Bundle CSS -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/framework7/framework7-bundle.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/fonts/fontawesome-pro/web/css/all.min.css" />
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/framework7/css/icons.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/framework7/css/my-app.css">
    <style>
    .message-name {
      margin-top: 15px !important;
    }
    </style>
  </head>
  <body>
    <div id="app">
      <div class="view view-main">
        <div class="page" data-name="chat" style="background-color: var(--f7-list-item-divider-bg-color);">
          <div class="navbar">
            <div class="navbar-bg"></div>
            <div class="navbar-inner">
              <div class="title"><?=$title?></div>
            </div>
          </div>
          <div class="toolbar messagebar" data-label="messagebar-chat">
            <div class="toolbar-inner">
              <!--<a class="link toggle-sheet" href="#">
                <i class="icon material-icons md-only">image</i>
              </a>-->
              <div class="messagebar-area">
                <textarea class="resizable" placeholder="Pesan Anda" data-label="input-chat"></textarea>
              </div>
              <a class="link send-link" href="#" data-label="btn-send"><i class="icon material-icons md-only">send</i></a>
            </div>
          </div>
          <div class="page-content messages-content">
            <div class="messages" data-label="list-chat">

            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="<?=base_url()?>assets/themes/framework7/framework7-bundle.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/js/moment.js"></script>
    <script type="text/javascript">
    function isJson(str) {
      try {
        JSON.parse(str);
      } catch (e) {
        return false;
      }
      return true;
    }

    function setLocalStorage(key, val) {
      localStorage.setItem(key, JSON.stringify(val));
    }

    function getLocalStorage(key) {
      var ls = localStorage.getItem(key);
      if(isJson(ls)) {
        return JSON.parse(ls);
      }
      else {
        return ls;
      }
    }

    function removeLocalStorage(key) {
      localStorage.removeItem(key);
    }

    function getData(action, data, successCB, failCB, doneCB) {
      var url = action;
      $.ajax({
        type: "POST",
        url: url,
        data: data,
        crossDomain: true,
        success: successCB,
        error: failCB,
        complete: function() {
          if(doneCB) (doneCB)();
        }
      });
    }

    function _loadChats(messages_) {
      var msgCurrent = getLocalStorage('chats');
      var msgNew = [];

      if(!messages_ || messages_.length == 0) {
        objMessages.clear();
        removeLocalStorage('chats');
        return;
      }

      for(var i=0; i<messages_.length; i++) {
        if(messages_[i].avatar) {
          messages_[i].avatar = '<?=MY_IMAGEURL?>'+'chat/'+messages_[i].avatar;
        }
      }

      if(msgCurrent) {
        for(var i=0; i<messages_.length; i++) {
          if(messages_[i].attrs) {
            var exist_ = msgCurrent.find(x => x.attrs.PesanID == messages_[i].attrs.PesanID);
            if(!exist_) {
              msgNew.push(messages_[i]);
            }
          }
        }

        if(msgNew && msgNew.length>0) {
          msgCurrent = msgCurrent.concat(msgNew);
        }
      } else {
        objMessages.clear();
        msgNew = messages_;
        msgCurrent = messages_;
      }

      setLocalStorage('chats', msgCurrent);
      if(objMessages.messages.length > 0) {
        objMessages.addMessages(msgNew);
      } else {
        objMessages.addMessages(msgCurrent);
      }
    }

    function _refreshChats() {
      getData('<?=site_url('site/api/message-load/1')?>', {UserName: '<?=$from?>', To: '<?=$to?>'}, function(res) {
        res = JSON.parse(res);
        if(res.error == 0) {
          if(res.data) {
            if(res.data.chats) {
              _loadChats(res.data.chats);
            }
          }
        } else {
          app.dialog.alert(res.error);
          return false;
        }
      }, function(){
        var toast = app.toast.create({
          text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
          icon: '<i class="icon material-icons md-only">error</i>',
          position: 'center',
          closeTimeout: 2000
        });
        toast.open();
      }, function(){

      });
    }

    removeLocalStorage('chats');

    var $$ = Dom7;
    var app = new Framework7({
      el: '#app',
      theme:'md',
      name: 'SAPA HASBIE',
      id: 'io.partopitao.sapahasbie',
    });

    var objMessagebar = app.messagebar.create({
      el: '.messagebar[data-label=messagebar-chat]'
    });
    var objMessages = app.messages.create({
      el: '.messages[data-label=list-chat]',
      firstMessageRule: function (message, previousMessage, nextMessage) {
        if (message.isTitle) return false;
        if (!previousMessage || previousMessage.type !== message.type || previousMessage.name !== message.name) return true;
        return false;
      },
      lastMessageRule: function (message, previousMessage, nextMessage) {
        if (message.isTitle) return false;
        if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
        return false;
      },
      tailMessageRule: function (message, previousMessage, nextMessage) {
        if (message.isTitle) return false;
        if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
        return false;
      },
      sameFooterMessageRule: function (message, previousMessage, nextMessage) {
        if (message.isTitle) return false;
        if (nextMessage && nextMessage.type == message.type && nextMessage.name == message.name) return true;
        return false;
      }
    });

    //var mainView = app.views.create('.view-main');
    _refreshChats();
    setInterval(function(){
      _refreshChats();
    }, 2000);

    $('[data-label=btn-send]').unbind('click').click(function(){
      var _from = '<?=$from?>';
      var _to = '<?=$to?>';
      var _text = objMessagebar.getValue().replace(/\n/g, '<br>').trim(); //$$('[data-label=input-chat]', ui.target).val();

      if (!_text.length) return false;

      getData('<?=site_url('site/api/message-send')?>', {From: _from, To: _to, Text: _text}, function(res) {
        res = JSON.parse(res);
        if(res.error == 0) {
          objMessagebar.clear();
          objMessagebar.focus();

          if(res.data) {
            _loadChats(res.data);
          }
        } else {
          app.dialog.alert(res.error);
          return false;
        }
      }, function(){
        var toast = app.toast.create({
          text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
          icon: '<i class="icon material-icons md-only">error</i>',
          position: 'center',
          closeTimeout: 2000
        });
        toast.open();
      }, function(){

      });
    });

    getData('<?=site_url('site/api/message-read')?>', {UserName: '<?=$from?>', To: '<?=$to?>'}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {

      } else {
        app.dialog.alert(res.error);
        return false;
      }
    }, function(){
      var toast = app.toast.create({
        text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
        icon: '<i class="icon material-icons md-only">error</i>',
        position: 'center',
        closeTimeout: 2000
      });
      toast.open();
    }, function(){

    });
    </script>
  </body>
</html>
