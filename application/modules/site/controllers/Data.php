<?php
class Data extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('user/dashboard');
    }
  }

  public function index($kat) {
    $data['kat'] = $kat;
    $data['title'] = 'Daftar '.ucwords($kat);

    $this->template->load('backend' , 'data/index', $data);
  }

  public function index_load($kat) {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_LAPID,COL_LAPTIPE,COL_NAME,COL_LAPJUDUL,COL_LAPSTATUS,COL_CREATEDON);
    $cols = array(COL_LAPID,COL_LAPTIPE,COL_NAME,COL_LAPJUDUL,COL_LAPSTATUS);

    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEMANAGER) {
      $this->db->like(COL_LAPPIC, $ruser[COL_USERNAME]);
    }

    $queryAll = $this->db
    ->where(COL_LAPKATEGORI, strtoupper($kat))
    ->get(TBL_TLAPORAN);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_ROLEID]!=ROLEMANAGER) {
      $this->db->like(COL_LAPPIC, $ruser[COL_USERNAME]);
    }
    if(!empty($filterStatus)) {
      $this->db->where(COL_LAPSTATUS, $filterStatus);
    }
    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select("tlaporan.*, mstatus.*, _userinformation.Name, _userinformation.Email")
    ->where(COL_LAPKATEGORI, strtoupper($kat))
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TLAPORAN.".".COL_CREATEDBY,"inner")
    ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_STATUSNAME." = ".TBL_TLAPORAN.".".COL_LAPSTATUS,"inner")
    ->order_by(TBL_MSTATUS.'.'.COL_STATUSSEQ, 'asc')
    ->get_compiled_select(TBL_TLAPORAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/data/form/'.$r[COL_LAPID]).'" class="btn btn-xs btn-outline-primary"><i class="fas fa-info-circle"></i>&nbsp;RINCIAN</a>&nbsp;';
      $data[] = array(
        $htmlBtn,
        str_pad($r[COL_LAPID],4,"0",STR_PAD_LEFT),
        $r[COL_LAPTIPE],
        $r[COL_NAME],
        $r[COL_LAPJUDUL],
        $r[COL_LAPSTATUS],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function form($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_LAPID, $id)
    ->get(TBL_TLAPORAN)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    if(!empty($_POST)) {
      $arrPIC = $this->input->post(COL_LAPPIC);

      $data = array(
        COL_LAPSTATUS=>$this->input->post(COL_LAPSTATUS),
        COL_LAPPIC=>$arrPIC?implode(",",$arrPIC):null
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_LAPID, $id)->update(TBL_TLAPORAN, $data);
        if(!$res) {
          throw new Exception('Terjadi kesalahan pada server.');
        }

        if($data[COL_LAPSTATUS]!=$rdata[COL_LAPSTATUS]) {
          $res = $this->db->insert(TBL_TLAPORANLOG, array(
            COL_LAPID=>$id,
            COL_LOGTIPE=>'STATUS',
            COL_LOGREMARKS=>$data[COL_LAPSTATUS],
            COL_LOGUSERNAME=>$ruser[COL_USERNAME],
            COL_LOGTIMESTAMP=>date('Y-m-d H:i:s')
          ));
          if(!$res) {
            throw new Exception('Terjadi kesalahan pada server.');
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess(ucwords($rdata[COL_LAPKATEGORI]).' berhasil diperbarui.', array('redirect'=>current_url()));
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      $data['data'] = $rdata;
      $data['title'] = ucwords($rdata[COL_LAPKATEGORI]).' #'.str_pad($rdata[COL_LAPID],4,"0",STR_PAD_LEFT);
      $this->template->load('backend' , 'data/form', $data);
    }
  }

  public function form_log($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_LAPID, $id)
    ->get(TBL_TLAPORAN)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "jpg|jpeg|png|pdf|doc|docx|xls|xlsx";
      /*$config['max_size']	= 5120;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;*/
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      $dat = array(
        COL_LAPID=>$id,
        COL_LOGTIPE=>'CATATAN',
        COL_LOGREMARKS=>$this->input->post(COL_LOGREMARKS),
        COL_LOGUSERNAME=>$ruser[COL_USERNAME],
        COL_LOGTIMESTAMP=>date("Y-m-d H:i:s")
      );

      $this->db->trans_begin();
      try {
        if(!empty($_FILES) && !empty($_FILES['file']['name'])) {
          $res = $this->upload->do_upload('file');
          if(!$res) {
            $err = $this->upload->display_errors('', '');
            throw new Exception($err);
          }

          $upl = $this->upload->data();
          $dat[COL_LOGFILE] = $upl['file_name'];
        }

        $res = $this->db->insert(TBL_TLAPORANLOG, $dat);
        if(!$res) {
          throw new Exception('Terjadi kesalahan pada server.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Catatan berhasil ditambahkan.');
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      $data['data'] = $rdata;
      $this->load->view('site/data/form-log', $data);
    }
  }
}
