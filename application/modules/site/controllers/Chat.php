<?php
class Chat extends MY_Controller {
  function __construct() {
    parent::__construct();
  }

  public function index() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      show_error('ANDA TIDAK MEMILIKI AKSES!');
      exit();
    }
    $data['title'] = "Pesanan";
    $this->template->load('backend', 'order/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $stat = !empty($_POST['filterChatStatus'])?$_POST['filterChatStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_TIMESTAMP,COL_PESANDARI,COL_PESANTEXT);
    $cols = array(COL_PESANDARI, COL_PESANTEXT);

    $queryAll = $this->db
    ->join(TBL__USERS.' from','from.'.COL_USERNAME." = ".TBL_TPESAN.".".COL_PESANDARI,"left")
    ->where(COL_PESANDARI." != ", $ruser[COL_USERNAME])
    ->where_in("from.".COL_ROLEID, array(ROLEPUBLIC, ROLEOPERATOR))
    ->group_by(COL_PESANDARI)
    ->get(TBL_TPESAN);


    $q = $this->db
    ->select('tpesan.PesanDari, MAX(Timestamp) as Timestamp')
    ->join(TBL__USERS.' from','from.'.COL_USERNAME." = ".TBL_TPESAN.".".COL_PESANDARI,"left")
    ->where(COL_PESANDARI." != ", $ruser[COL_USERNAME])
    ->where_in("from.".COL_ROLEID, array(ROLEPUBLIC, ROLEOPERATOR))
    ->order_by(COL_TIMESTAMP, 'desc')
    ->group_by(COL_PESANDARI)
    ->get_compiled_select(TBL_TPESAN);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $rpesan = $this->db
      ->select('tpesan.*, from.Name as Name, from.Email as Email')
      ->where(COL_PESANDARI, $r[COL_PESANDARI])
      ->join(TBL__USERINFORMATION.' from','from.'.COL_USERNAME." = ".TBL_TPESAN.".".COL_PESANDARI,"left")
      ->order_by(COL_TIMESTAMP, 'desc')
      ->get(TBL_TPESAN)
      ->row_array();

      $runread = $this->db
      ->where(COL_PESANDARI, $r[COL_PESANDARI])
      ->where(COL_PESANISREAD, 0)
      ->get(TBL_TPESAN)
      ->result_array();

      $rtotal = $this->db
      ->where(COL_PESANDARI, $r[COL_PESANDARI])
      ->count_all_results(TBL_TPESAN);

      $chat_ = !empty($rpesan)?strip_tags($rpesan[COL_PESANTEXT]):'-';

      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/chat/form/'.$r[COL_PESANDARI]).'" class="btn btn-xs btn-outline-primary btn-chat-open"><i class="far fa-comment-alt-dots"></i>&nbsp;CHAT</a>&nbsp;';
      $dat = array(
        $htmlBtn,
        date('Y-m-d H:i', strtotime($r[COL_TIMESTAMP])),
        !empty($rpesan)?$rpesan["Email"]:"-",
        !empty($rpesan)?$rpesan["Name"]:"-",
        strlen($chat_) > 100 ? substr($chat_, 0, 100) . "..." : $chat_,
        '<span class="badge bg-'.(count($runread)>0?'danger':'gray').'">'.number_format(count($runread)).'</span>',
        '<span class="badge bg-olive">'.number_format($rtotal).'</span>',
      );
      if(!empty($runread)) {
        for($i=1; $i<count($dat);$i++) {
          $dat[$i] = '<strong>'.$dat[$i].'</strong>';
        }
      }

      $data[] = $dat;
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $queryAll->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function form($uname) {
    $ruser = GetLoggedUser();
    $to = $this->db
    ->where(COL_USERNAME, $uname)
    ->get(TBL__USERINFORMATION)
    ->row_array();

    if(empty($to)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $data['title'] = 'LIVE CHAT - '.$to[COL_NAME];
    $data['to'] = $uname;
    $data['from'] = $ruser[COL_USERNAME];
    $this->load->view('site/chat/form', $data);
  }
}
?>
