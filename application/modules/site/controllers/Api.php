<?php
class Api extends MY_Controller {
  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
  }

  private function _loadchats($_from, $isadmin=false, $_user='') {
    $rmessage = $this->db
    ->select("tpesan.*, userFrom.Name as FromName, userFrom_.RoleID as FromRole, userTo.Name as ToName, userTo_.RoleID as ToRole")
    ->join(TBL__USERINFORMATION.' userFrom','userFrom.'.COL_USERNAME." = ".TBL_TPESAN.".".COL_PESANDARI,"left")
    ->join(TBL__USERS.' userFrom_','userFrom_.'.COL_USERNAME." = ".TBL_TPESAN.".".COL_PESANDARI,"left")
    ->join(TBL__USERINFORMATION.' userTo','userTo.'.COL_USERNAME." = ".TBL_TPESAN.".".COL_PESANKE,"left")
    ->join(TBL__USERS.' userTo_','userTo_.'.COL_USERNAME." = ".TBL_TPESAN.".".COL_PESANDARI,"left")
    ->where("(PesanDari = '$_from' or PesanKe = '$_from')")
    ->order_by(COL_TIMESTAMP, 'asc')
    ->get(TBL_TPESAN)
    ->result_array();

    $arrMessage = array();
    $date = '';
    foreach($rmessage as $m) {
      if(date('Y-m-d', strtotime($m[COL_TIMESTAMP])) != $date) {
        $date = date('Y-m-d', strtotime($m[COL_TIMESTAMP]));
        $arrMessage[] = array(
          'text'=>date('d-m-Y', strtotime($m[COL_TIMESTAMP])),
          'isTitle'=>true,
          'attrs'=>array('PesanID'=>date('d-m-Y', strtotime($m[COL_TIMESTAMP])), 'PesanIsRead'=>true)
        );
      }

      $avatar = 'img/icon-avatar-user.png';
      if($m[COL_PESANDARI]==$_from) {
        if($m['FromRole']!=ROLEPUBLIC) $avatar = 'img/icon-avatar-operator.png';
      } else {
        if($m['ToRole']!=ROLEPUBLIC) $avatar = 'img/icon-avatar-operator.png';
      }

      $type = 'sent';
      $name = $m['FromName'];
      if($m[COL_PESANDARI]==$_from) {
        if($isadmin) {
          $name = !empty($m['FromName'])?$m['FromName']:'Anonim';
          $type = 'received';
        } else {
          $name = 'Anda';
          $type = 'sent';
        }
      } else {
        if($isadmin) {
          $name = !empty($_user)&&$_user!=$m[COL_PESANDARI]?$m['FromName']:'Anda';
          $type = 'sent';
        } else {
          $name = !empty($m['FromName'])?$m['FromName']:'Anonim';
          $type = 'received';
        }
      }

      $arrMessage[] = array(
        'text'=>$m[COL_PESANTEXT],
        'footer'=>date('H:i', strtotime($m[COL_TIMESTAMP])),
        'name'=>$name,//$m[COL_PESANDARI]==$_from?'Anda':(!empty($m['FromName'])?$m['FromName']:'Anonim'),
        'type'=>$type,//$m[COL_PESANDARI]==$_from?('sent'):'received',
        'avatar'=>$avatar,//$m[COL_PESANDARI]==$_from?'img/icon-avatar-user.png':($m['FromRole']!=ROLEPUBLIC?'img/icon-avatar-operator.png':'img/icon-avatar-user.png'),
        'attrs'=>array('PesanID'=>$m[COL_PESANID], 'PesanIsRead'=>$m[COL_PESANISREAD])
      );
    }

    return $arrMessage;
  }

  public function login() {
    $this->load->model('muser');
    $json = file_get_contents('php://input');
    $data = json_decode($json);

    if(empty($json)) {
      ShowJsonError('JSON tidak valid.');
      return;
    }

    $username = $this->input->post('username');
    $password = $this->input->post('password');

    if($this->muser->authenticate($username, $password)) {
      if($this->muser->IsSuspend($username)) {
        ShowJsonError('Akun anda di suspend.');
        return;
      }

      $this->db->where(COL_USERNAME, $username);
      $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

      $userdetails = $this->muser->getdetails($username);
      //$token = GetEncryption($userdetails[COL_USERNAME]);
      ShowJsonSuccess('Login berhasil.', array('data'=>array(
        'Email' => $userdetails[COL_USERNAME],
        'Role' => !empty($userdetails[COL_IDENTITYNO])?$userdetails[COL_IDENTITYNO]:$userdetails[COL_ROLENAME],
        'RoleID' => $userdetails[COL_ROLEID],
        'Nama' => $userdetails[COL_NAME],
        'Address' => $userdetails[COL_ADDRESS]
        //'Token' => $token)));
      )));
    } else {
      ShowJsonError('Username / password tidak tepat.');
    }
  }

  public function register() {
    $this->load->model('muser');
    $json = file_get_contents('php://input');
    $data = json_decode($json);

    if(empty($json)) {
      ShowJsonError('JSON tidak valid.');
      return;
    }

    $rcheck = $this->db
    ->where(COL_USERNAME, $this->input->post('phoneno'))
    ->get(TBL__USERS)
    ->row_array();
    if(!empty($rcheck)) {
      ShowJsonError('Maaf, nomor yang anda masukkan sudah terdaftar di sistem.');
      return;
    }

    $userdata = array(
      COL_USERNAME => $this->input->post('phoneno'),
      COL_PASSWORD => md5($this->input->post('password')),
      COL_ROLEID => ROLEPUBLIC,
      COL_ISSUSPEND => false
    );
    $userinfo = array(
      COL_USERNAME => $this->input->post('phoneno'),
      COL_EMAIL => $this->input->post('phoneno'),
      COL_NAME => $this->input->post('name'),
      COL_ADDRESS => $this->input->post('address'),
      COL_PHONENUMBER => $this->input->post('phoneno'),
      COL_REGISTEREDDATE => date('Y-m-d')
    );

    $reg = $this->muser->register($userdata, $userinfo, null);
    if(!$reg) {
      ShowJsonError('Registrasi gagal! Silakan coba kembali beberapa saat lagi.');
      exit();
    }

    ShowJsonSuccess('Registrasi berhasil. Silakan login menggunakan No. HP anda.');
    exit();
  }

  public function get_opt_tipe($kat) {
    $resp = array();
    $res = $this->db
    ->where(TBL_MTIPE.'.'.COL_KATEGORI, strtoupper($kat))
    ->order_by(TBL_MTIPE.'.'.COL_SEQ, 'asc')
    ->get(TBL_MTIPE)
    ->result_array();

    if($res) {
      foreach($res as $r) {
        $resp[] = array(
          'Value'=>$r[COL_TIPE],
          'Text'=>$r[COL_TIPE]
        );
      }
    }
    ShowJsonSuccess('Loaded.', array('data'=>array('opts'=>$resp)));
  }

  public function laporan_add() {
    $rdata = array();
    $arrAttachment = array();
    $files = $_FILES;
    $cpt = !empty($_FILES)?count($_FILES['file']['name']):0;

    if($cpt > 0) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "gif|jpg|jpeg|png|pdf|mp4|avi|mkv|wav|webm|flv|wmv|mov";
      $config['max_size'] = 102400;
      $config['overwrite'] = FALSE;
      $this->load->library('upload',$config);

      for($i=0; $i<$cpt; $i++)
      {
        if(!empty($files['file']['name'][$i])) {
          $_FILES['file']['name']= $files['file']['name'][$i];
          $_FILES['file']['type']= $files['file']['type'][$i];
          $_FILES['file']['tmp_name']= $files['file']['tmp_name'][$i];
          $_FILES['file']['error']= $files['file']['error'][$i];
          $_FILES['file']['size']= $files['file']['size'][$i];

          $res = $this->upload->do_upload('file');
          if(!$res) {
            ShowJsonError(strip_tags($this->upload->display_errors()));
            exit();
          }

          $upl = $this->upload->data();
          $arrAttachment[] = $upl['file_name'];
        }
      }
    }

    /*if(!empty($_FILES['file'])) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "gif|jpg|jpeg|png|pdf|mp4|avi|mkv|wav|webm|flv|wmv|mov";
      $config['max_size'] = 102400;
      $config['overwrite'] = FALSE;
      $this->load->library('upload',$config);

      $res = $this->upload->do_upload('file');
      if(!$res) {
        ShowJsonError(strip_tags($this->upload->display_errors()));
        exit();
      }

      $upl = $this->upload->data();
      $dat[COL_LAPFILE] = $upl['file_name'];
    }*/

    $dat = array(
      COL_LAPKATEGORI=>strtoupper($this->input->post(COL_LAPKATEGORI)),
      COL_LAPTIPE=>$this->input->post(COL_LAPTIPE),
      COL_LAPJUDUL=>$this->input->post(COL_LAPJUDUL),
      COL_LAPKETERANGAN=>$this->input->post(COL_LAPKETERANGAN),
      COL_LAPFILE=>!empty($arrAttachment)?implode(",", $arrAttachment):null,
      COL_LAPLOKASI=>$this->input->post(COL_LAPLOKASI),
      COL_LAPTANGGAL=>$this->input->post(COL_LAPTANGGAL),
      COL_CREATEDBY=>$this->input->post(COL_CREATEDBY),
      COL_CREATEDON=>date('Y-m-d H:i:s')
    );

    $id='';
    $this->db->trans_begin();
    try {
      $res = $this->db->insert(TBL_TLAPORAN, $dat);
      if(!$res) {
        throw new Exception('Terjadi kesalahan pada server.');
      }

      $id = $this->db->insert_id();
      $res = $this->db->insert(TBL_TLAPORANLOG, array(
        COL_LAPID=>$id,
        COL_LOGTIPE=>'STATUS',
        COL_LOGREMARKS=>'BARU',
        COL_LOGUSERNAME=>'SYSTEM',
        COL_LOGTIMESTAMP=>date('Y-m-d H:i:s')
      ));
      if(!$res) {
        throw new Exception('Terjadi kesalahan pada server.');
      }
      $this->db->trans_commit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      exit();
    }

    $rdata = $this->db
    ->where(COL_LAPID, $id)
    ->get(TBL_TLAPORAN)
    ->row_array();
    ShowJsonSuccess(ucwords($this->input->post(COL_LAPKATEGORI)).' No. <strong>'.str_pad($id,4,"0",STR_PAD_LEFT).'</strong> berhasil ditambahkan.', array('data'=>$rdata));
  }

  public function laporan($kat) {
    $uname = $this->input->post(COL_USERNAME);
    $dateFrom = $this->input->post('dateFrom');
    $dateTo = $this->input->post('dateTo');
    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      //ShowJsonError('Autentikasi gagal.');
      //exit();
    }

    if(!empty($dateFrom)&&!empty($dateTo)) {
      $this->db->where("DATE(".TBL_TLAPORAN.'.'.COL_CREATEDON.")>=", $dateFrom);
      $this->db->where("DATE(".TBL_TLAPORAN.'.'.COL_CREATEDON.")<=", $dateTo);
    }
    if(empty($ruser) || $ruser[COL_ROLEID]==ROLEPUBLIC) {
      $this->db->where(COL_CREATEDBY, $uname);
    }
    if(empty($ruser) || $ruser[COL_ROLEID]==ROLEOPERATOR) {
      $this->db->like(TBL_TLAPORAN.'.'.COL_LAPPIC, $ruser[COL_USERNAME]);
    }
    $res = $this->db
    ->select('tlaporan.*, _userinformation.Name as LapNama')
    ->where(TBL_TLAPORAN.'.'.COL_LAPKATEGORI, strtoupper($kat))
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TLAPORAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_STATUSNAME." = ".TBL_TLAPORAN.".".COL_LAPSTATUS,"inner")
    ->order_by(TBL_MSTATUS.'.'.COL_STATUSSEQ, 'asc')
    ->order_by(TBL_TLAPORAN.'.'.COL_CREATEDON, 'desc')
    ->get(TBL_TLAPORAN)
    ->result_array();
    ShowJsonSuccess('Loaded.', array('data'=>$res));
  }

  public function laporan_detail($id) {
    $res = $this->db
    ->select('tlaporan.*, _userinformation.Name as LapNama')
    ->where(TBL_TLAPORAN.'.'.COL_LAPID, $id)
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TLAPORAN.".".COL_CREATEDBY,"left")
    ->get(TBL_TLAPORAN)
    ->row_array();

    if(empty($res)) {
      ShowJsonError('Parameter tidak valid.');
      exit();
    }

    if(!empty($res[COL_LAPFILE])) {
      $files = explode(",", $res[COL_LAPFILE]);
      $file_ = array();
      foreach($files as $f) {
        if(file_exists(MY_UPLOADPATH.$f)) $file_[] = MY_UPLOADURL.$f;
      }

      $res[COL_LAPFILE] = implode(",", $file_);//MY_UPLOADURL.$res[COL_LAPFILE];
    }

    $res2 = $this->db
    ->select("tlaporanlog.*, COALESCE(_userinformation.Name, tlaporanlog.LogUserName) as LogName, '' as LogDescription, tlaporan.LapKategori")
    ->where(TBL_TLAPORANLOG.'.'.COL_LAPID, $id)
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TLAPORANLOG.".".COL_LOGUSERNAME,"left")
    ->join(TBL_TLAPORAN,TBL_TLAPORAN.'.'.COL_LAPID." = ".TBL_TLAPORANLOG.".".COL_LAPID,"left")
    ->order_by(TBL_TLAPORANLOG.'.'.COL_LOGTIMESTAMP, 'desc')
    ->get(TBL_TLAPORANLOG)
    ->result_array();

    if($res2) {
      for($i=0; $i<count($res2); $i++) {
        if($res2[$i][COL_LOGTIPE]=='STATUS') {
          $res2[$i]['LogDescription'] = 'Status berubah menjadi <strong>'.$res2[$i][COL_LOGREMARKS].'</strong>.';
          if($res2[$i][COL_LOGREMARKS]=='BARU') {
            $res2[$i]['LogDescription'] = $res2[$i][COL_LAPKATEGORI].' <strong>'.str_pad($res2[$i][COL_LAPID],4,"0",STR_PAD_LEFT).'</strong> diterima.';
          }
        } else if($res2[$i][COL_LOGTIPE]=='CATATAN') {
          $res2[$i]['LogDescription'] = $res2[$i][COL_LOGREMARKS];
        }

        if(!empty($res2[$i][COL_LOGFILE])) {
          $files_ = array();
          $files = explode(",", $res2[$i][COL_LOGFILE]);
          foreach($files as $f) {
            $files_[] = MY_UPLOADURL.$f;
          }
          $res2[$i][COL_LOGFILE] = implode(",", $files_);//MY_UPLOADURL.$res2[$i][COL_LOGFILE];
        }
      }
    }

    ShowJsonSuccess('Loaded.', array('data'=>array('laporan'=>$res, 'logs'=>$res2)));
  }

  public function message_send() {
    $ruser = array();
    $isadmin = false;
    $uname = $this->input->post('From');
    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      //ShowJsonError('Autentikasi gagal.');
      //exit();
    }

    if(!empty($ruser) && $ruser[COL_ROLEID] != ROLEPUBLIC) $isadmin = true;

    $_from = $this->input->post("From");
    $_to = $this->input->post("To");
    $_text = $this->input->post("Text");

    $dat = array(
      COL_PESANDARI=>$_from,
      COL_PESANKE=>!empty($_to)?$_to:null,
      COL_PESANTEXT=>$_text,
      COL_TIMESTAMP=>date('Y-m-d H:i:s')
    );

    $res = $this->db->insert(TBL_TPESAN, $dat);
    if(!$res) {
      ShowJsonError('Terjadi kesalahan pada server.');
      exit();
    }

    $arrMessage = $this->_loadchats(($isadmin?$_to:$_from), $isadmin, ($isadmin?$_from:''));
    ShowJsonSuccess('Success', array('data'=>$arrMessage));
  }

  public function message_load($forceRead=false) {
    $ruser = array();
    $isadmin = false;
    $uname = $this->input->post(COL_USERNAME);
    $to = $this->input->post('To');

    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      //ShowJsonError('Autentikasi gagal.');
      //exit();
    }

    if(!empty($ruser) && $ruser[COL_ROLEID] != ROLEPUBLIC) $isadmin = true;

    $arrMessage = $this->_loadchats(($isadmin?$to:$uname)/*$uname*/, $isadmin, ($isadmin?$uname:''));
    $arrUnread = array();

    foreach($arrMessage as $m) {
      if($m['attrs'][COL_PESANISREAD]==0 && $m['type']=='received') $arrUnread[] = $m;
    }

    if($forceRead) {
      $res = $this->db->where(($isadmin?"PesanDari = '$to'":"PesanKe = '$uname'"))->update(TBL_TPESAN, array(COL_PESANISREAD=>1));
      if(!$res) {
        ShowJsonError('Terjadi kesalahan pada server.');
        exit();
      }
    }

    $rnotif = 0;
    $rnotif_laporan = 0;
    $rnotif_aduan = 0;
    if(!empty($ruser) && $ruser[COL_ROLEID] != ROLEPUBLIC) {
      if($ruser[COL_ROLEID] == ROLEOPERATOR) {
        $this->db->like(TBL_TLAPORAN.'.'.COL_LAPPIC, $ruser[COL_USERNAME]);
      }
      $rnotif = $this->db
      ->select('tlaporan.*, _userinformation.Name as LapNama')
      ->where_not_in(TBL_TLAPORAN.'.'.COL_LAPSTATUS, array('SELESAI', 'DITOLAK'))
      //->like(TBL_TLAPORAN.'.'.COL_LAPPIC, $ruser[COL_USERNAME])
      ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TLAPORAN.".".COL_CREATEDBY,"left")
      ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_STATUSNAME." = ".TBL_TLAPORAN.".".COL_LAPSTATUS,"inner")
      ->order_by(TBL_MSTATUS.'.'.COL_STATUSSEQ, 'asc')
      ->order_by(TBL_TLAPORAN.'.'.COL_CREATEDON, 'desc')
      ->count_all_results(TBL_TLAPORAN);

      if($ruser[COL_ROLEID] == ROLEOPERATOR) {
        $this->db->like(TBL_TLAPORAN.'.'.COL_LAPPIC, $ruser[COL_USERNAME]);
      }
      $rnotif_laporan = $this->db
      ->select('tlaporan.*, _userinformation.Name as LapNama')
      ->where(TBL_TLAPORAN.'.'.COL_LAPKATEGORI, 'LAPORAN')
      ->where_not_in(TBL_TLAPORAN.'.'.COL_LAPSTATUS, array('SELESAI', 'DITOLAK'))
      //->like(TBL_TLAPORAN.'.'.COL_LAPPIC, $ruser[COL_USERNAME])
      ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TLAPORAN.".".COL_CREATEDBY,"left")
      ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_STATUSNAME." = ".TBL_TLAPORAN.".".COL_LAPSTATUS,"inner")
      ->order_by(TBL_MSTATUS.'.'.COL_STATUSSEQ, 'asc')
      ->order_by(TBL_TLAPORAN.'.'.COL_CREATEDON, 'desc')
      ->count_all_results(TBL_TLAPORAN);

      if($ruser[COL_ROLEID] == ROLEOPERATOR) {
        $this->db->like(TBL_TLAPORAN.'.'.COL_LAPPIC, $ruser[COL_USERNAME]);
      }
      $rnotif_aduan = $this->db
      ->select('tlaporan.*, _userinformation.Name as LapNama')
      ->where(TBL_TLAPORAN.'.'.COL_LAPKATEGORI, 'ADUAN')
      ->where_not_in(TBL_TLAPORAN.'.'.COL_LAPSTATUS, array('SELESAI', 'DITOLAK'))
      //->like(TBL_TLAPORAN.'.'.COL_LAPPIC, $ruser[COL_USERNAME])
      ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TLAPORAN.".".COL_CREATEDBY,"left")
      ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_STATUSNAME." = ".TBL_TLAPORAN.".".COL_LAPSTATUS,"inner")
      ->order_by(TBL_MSTATUS.'.'.COL_STATUSSEQ, 'asc')
      ->order_by(TBL_TLAPORAN.'.'.COL_CREATEDON, 'desc')
      ->count_all_results(TBL_TLAPORAN);
    }

    ShowJsonSuccess('Success', array('data'=>array('chats'=>$arrMessage, 'unread'=>$arrUnread, 'notifs'=>$rnotif, 'notif1'=>$rnotif_laporan, 'notif2'=>$rnotif_aduan)));
  }

  public function message_read() {
    $ruser = array();
    $isadmin = false;
    $uname = $this->input->post(COL_USERNAME);
    $to = $this->input->post('To');

    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      //ShowJsonError('Autentikasi gagal.');
      //exit();
    }

    if(!empty($ruser) && $ruser[COL_ROLEID] != ROLEPUBLIC) $isadmin = true;

    $res = $this->db->where(($isadmin?"PesanDari = '$to'":"PesanKe = '$uname'"))->update(TBL_TPESAN, array(COL_PESANISREAD=>1));
    if(!$res) {
      ShowJsonError('Terjadi kesalahan pada server.');
      exit();
    }

    ShowJsonSuccess('Success', array('data'=>array('query'=>$this->db->last_query())));
  }

  public function message_count() {
    $rpesan = $this->db
    ->where('from.RoleID', ROLEPUBLIC)
    ->where("DATE(Timestamp)", date('Y-m-d'))
    ->join(TBL__USERS.' from','from.'.COL_USERNAME." = ".TBL_TPESAN.".".COL_PESANDARI,"left")
    ->count_all_results(TBL_TPESAN);

    ShowJsonSuccess('Loaded', array('data'=>$rpesan));
  }

  public function notification_load($tipe) {
    $uname = $this->input->post(COL_USERNAME);
    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      ShowJsonSuccess('Loaded.', array('data'=>array()));
      exit();
    }

    $arrnotif = array();
    if($tipe=='opr') {
      $res = $this->db
      ->select('tlaporan.*, _userinformation.Name as LapNama')
      ->where_not_in(TBL_TLAPORAN.'.'.COL_LAPSTATUS, array('SELESAI', 'DITOLAK'))
      ->like(TBL_TLAPORAN.'.'.COL_LAPPIC, $ruser[COL_USERNAME])
      ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TLAPORAN.".".COL_CREATEDBY,"left")
      ->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_STATUSNAME." = ".TBL_TLAPORAN.".".COL_LAPSTATUS,"inner")
      ->order_by(TBL_MSTATUS.'.'.COL_STATUSSEQ, 'asc')
      ->order_by(TBL_TLAPORAN.'.'.COL_CREATEDON, 'desc')
      ->get(TBL_TLAPORAN)
      ->result_array();

      foreach($res as $r){
        $arrnotif[] = array(
          'ID'=>$r[COL_LAPID],
          'kat'=>$r[COL_LAPKATEGORI],
          'title'=>$r[COL_LAPTIPE],
          'subtitle'=>date('d-m-Y', strtotime($r[COL_CREATEDON])),
          'text'=>'Silakan klik untuk melihat rincian.'
        );
      }
    } else {

    }

    ShowJsonSuccess('Loaded.', array('data'=>$arrnotif));
  }

  public function log_add($id) {
    $uname = $this->input->post(COL_LOGUSERNAME);
    $ruser = $this->db
    ->where(COL_USERNAME,$uname)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruser)) {
      ShowJsonError('Autentikasi gagal.');
      exit();
    }

    $rdata = $this->db
    ->where(COL_LAPID, $id)
    ->get(TBL_TLAPORAN)
    ->row_array();

    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    if(!empty($_POST)) {
      $arrAttachment = array();
      $files = $_FILES;
      $cpt = !empty($_FILES)?count($_FILES['file']['name']):0;

      if($cpt > 0) {
        $config['upload_path'] = MY_UPLOADPATH;
        $config['allowed_types'] = "gif|jpg|jpeg|png|pdf|mp4|avi|mkv|wav|webm|flv|wmv|mov";
        $config['max_size'] = 102400;
        $config['overwrite'] = FALSE;
        $this->load->library('upload',$config);

        for($i=0; $i<$cpt; $i++)
        {
          if(!empty($files['file']['name'][$i])) {
            $_FILES['file']['name']= $files['file']['name'][$i];
            $_FILES['file']['type']= $files['file']['type'][$i];
            $_FILES['file']['tmp_name']= $files['file']['tmp_name'][$i];
            $_FILES['file']['error']= $files['file']['error'][$i];
            $_FILES['file']['size']= $files['file']['size'][$i];

            $res = $this->upload->do_upload('file');
            if(!$res) {
              ShowJsonError(strip_tags($this->upload->display_errors()));
              exit();
            }

            $upl = $this->upload->data();
            $arrAttachment[] = $upl['file_name'];
          }
        }
      }

      $dat = array(
        COL_LAPID=>$id,
        COL_LOGTIPE=>'CATATAN',
        COL_LOGREMARKS=>$this->input->post(COL_LOGREMARKS),
        COL_LOGUSERNAME=>$this->input->post(COL_LOGUSERNAME),
        COL_LOGFILE=>!empty($arrAttachment)?implode(",", $arrAttachment):null,
        COL_LOGTIMESTAMP=>date("Y-m-d H:i:s")
      );

      $res = $this->db->insert(TBL_TLAPORANLOG, $dat);
      if(!$res) {
        ShowJsonError('Terjadi kesalahan pada server.');
        exit();
      }

      ShowJsonSuccess("Catatan berhasil ditambahkan.");
      exit();
    } else {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }
  }
}
