# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: tt_aduanlh
# Generation Time: 2022-11-02 02:25:05 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _homepage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_homepage`;

CREATE TABLE `_homepage` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ContentID` varchar(20) DEFAULT NULL,
  `ContentTitle` varchar(200) DEFAULT NULL,
  `ContentType` varchar(50) NOT NULL,
  `ContentDesc1` text,
  `ContentDesc2` text,
  `ContentDesc3` text,
  `ContentDesc4` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_homepage` WRITE;
/*!40000 ALTER TABLE `_homepage` DISABLE KEYS */;

INSERT INTO `_homepage` (`Uniq`, `ContentID`, `ContentTitle`, `ContentType`, `ContentDesc1`, `ContentDesc2`, `ContentDesc3`, `ContentDesc4`)
VALUES
	(1,'TxtWelcome1','Sambutan & Foto','TEXT','<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','k_dinkes2.png',NULL,NULL),
	(2,'TxtWelcome2','Struktur Organisasi','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','STRUKTURORGANISASI.jpeg',NULL,NULL),
	(41,'NumProfile','NUMBER 1','NUMBER','Angka 1',NULL,'1','Satuan'),
	(42,'NumProfile','NUMBER 2','NUMBER','Angka 2',NULL,'2','Satuan'),
	(43,'NumProfile','NUMBER 3','NUMBER','Angka 3',NULL,'3','Satuan'),
	(44,'NumProfile','NUMBER 4','NUMBER','Angka 4',NULL,'4','Satuan'),
	(45,'TxtPopup1','Tugas Pokok dan Fungsi','PDF',NULL,'rekomsaranakesehatan.pdf',NULL,NULL),
	(46,'TxtPopup1','Maklumat Pelayanan','IMG',NULL,'rekomsaranakesehatan1.pdf',NULL,NULL),
	(47,'TxtPopup1','Standar Pelayanan','PDF',NULL,'rekomsaranakesehatan2.pdf',NULL,NULL),
	(49,'TxtPopup2','Kesehatan Masyarakat','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(50,'TxtPopup2','Pencegahan & Pengendalian Penyakit','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(51,'TxtPopup2','Pelayanan & Sumber Daya Kesehatan','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(52,'TxtPopup3','Instalasi Farmasi','DB','select * from munit where UnitTipe = \'FARMASI\' order by UnitNama','UnitNama','Uniq',NULL),
	(53,'TxtPopup3','Puskesmas','DB','select * from munit where UnitTipe = \'PUSKESMAS\' order by UnitNama','UnitNama','Uniq',NULL),
	(55,'Carousel','Carousel 1','IMG',NULL,NULL,NULL,NULL),
	(56,'Carousel','Carousel 2','IMG',NULL,NULL,NULL,NULL),
	(57,'Carousel','Carousel 3','IMG',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_homepage` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_logs`;

CREATE TABLE `_logs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Timestamp` datetime DEFAULT NULL,
  `URL` text,
  `ClientInfo` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_logs` WRITE;
/*!40000 ALTER TABLE `_logs` DISABLE KEYS */;

INSERT INTO `_logs` (`Uniq`, `Timestamp`, `URL`, `ClientInfo`)
VALUES
	(1,'2022-10-31 21:07:55','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(2,'2022-10-31 21:07:57','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(3,'2022-10-31 21:07:59','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(4,'2022-10-31 21:08:01','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(5,'2022-10-31 21:08:03','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(6,'2022-10-31 21:08:06','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(7,'2022-10-31 21:08:07','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(8,'2022-10-31 21:08:10','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(9,'2022-10-31 21:08:11','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(10,'2022-10-31 21:08:13','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(11,'2022-10-31 21:08:15','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(12,'2022-10-31 21:08:17','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(13,'2022-10-31 21:08:19','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(14,'2022-10-31 21:08:22','http://localhost/tt-aduanlh/site/api/message-load.jsp','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1');

/*!40000 ALTER TABLE `_logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(1,'Berita','#f56954',1,0,0),
	(2,'Infografis','#00a65a',0,0,0),
	(3,'Dokumen','#f39c12',0,0,1),
	(5,'Lainnya','#3c8dbc',1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(20) NOT NULL,
  `ImgPath` text NOT NULL,
  `ImgDesc` varchar(250) NOT NULL,
  `ImgShortcode` varchar(50) NOT NULL,
  `IsHeader` tinyint(1) NOT NULL DEFAULT '1',
  `IsThumbnail` tinyint(1) NOT NULL DEFAULT '1',
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(11) NOT NULL,
  `PostUnitID` int(11) DEFAULT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostExpiredDate` date DEFAULT NULL,
  `PostMetaTags` text,
  `IsRunningText` tinyint(1) NOT NULL DEFAULT '0',
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL,
  `RoleName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Operator'),
	(3,'Publik'),
	(4,'Manager');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `SettingID` int(10) unsigned NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`Uniq`, `SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,1,'SETTING_WEB_NAME','SETTING_WEB_NAME','SAPA HASBIE'),
	(2,2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Administrasi Pelaporan dan Aduan'),
	(3,3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,4,'SETTING_ORG_NAME','SETTING_ORG_NAME','Dinas Lingkungan Hidup Kota Tebing Tinggi'),
	(5,5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','--'),
	(9,9,'SETTING_ORG_FAX','SETTING_ORG_FAX','--'),
	(10,10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','--'),
	(11,11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(14,14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','main.gif'),
	(15,15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.01'),
	(16,16,'SETTING_ORG_REGION','SETTING_ORG_REGION','PEMERINTAH KOTA TEBING TINGGI');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(11) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(11) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(11) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`Uniq`, `UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`)
VALUES
	(1,'admin','administrator@dinkes.tebingtinggikota.go.id',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),
	(4,'197607312008011001','197607312008011001',NULL,'Dr. H. MUHAMMAD HASBIE ASHSHIDDIQI, M.M., M.Si','Kepala Dinas',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,'197502022000031003','197502022000031003',NULL,'RONGGUR RAJA BOLON SIBARANI, SKM','Sekretaris Dinas',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(6,'198303032008012004','198303032008012004',NULL,'UMI SOFYANI, S.E','Kasubbag Umum dan Kepegawaian',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(7,'198209152005021002','198209152005021002',NULL,'SYAHPUTRA, S.T','Kabid Penataan,Penaatan dan Peningkatan Kapasitas',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(8,'198110032006041008','198110032006041008',NULL,'ZULHADIN, SH','Kabid Pengelolaan Limbah B3, Kebersihan dan Ruang ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(9,'199010102019032015','199010102019032015',NULL,'RIBKA TIWA NAIBAHO, S.Si','Analis Lingkungan Hidup',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(10,'199410152019031007','199410152019031007',NULL,'ANGGI OTARI SIHITE, S.T','Analis Adaptasi Dampak Perubahan Iklim',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(11,'085359867032','085359867032',NULL,'Partopi Tao',NULL,NULL,NULL,NULL,'Middle of Nowhere','085359867032',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022-10-31');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`Uniq`, `UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	(1,'admin','88e8c22a727a1b7fa893317adb976787',1,0,'2022-11-01 23:28:36','::1'),
	(4,'197607312008011001','e10adc3949ba59abbe56e057f20f883e',4,0,'2022-10-31 21:45:31','::1'),
	(5,'197502022000031003','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	(6,'198303032008012004','e10adc3949ba59abbe56e057f20f883e',2,0,NULL,NULL),
	(7,'198209152005021002','e10adc3949ba59abbe56e057f20f883e',2,0,'2022-11-01 23:29:02','::1'),
	(8,'198110032006041008','e10adc3949ba59abbe56e057f20f883e',2,0,'2022-10-31 22:46:17','::1'),
	(9,'199010102019032015','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),
	(10,'199410152019031007','e10adc3949ba59abbe56e057f20f883e',4,0,'2022-11-01 20:18:17','::1'),
	(11,'085359867032','e10adc3949ba59abbe56e057f20f883e',3,0,'2022-11-01 23:46:51','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mstatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mstatus`;

CREATE TABLE `mstatus` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `StatusSeq` int(11) DEFAULT NULL,
  `StatusName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mstatus` WRITE;
/*!40000 ALTER TABLE `mstatus` DISABLE KEYS */;

INSERT INTO `mstatus` (`Uniq`, `StatusSeq`, `StatusName`)
VALUES
	(1,1,'BARU'),
	(2,2,'DIPROSES'),
	(3,3,'SELESAI'),
	(4,4,'DITOLAK');

/*!40000 ALTER TABLE `mstatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mtipe
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtipe`;

CREATE TABLE `mtipe` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Kategori` varchar(100) DEFAULT '',
  `Tipe` varchar(100) DEFAULT NULL,
  `Seq` int(11) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mtipe` WRITE;
/*!40000 ALTER TABLE `mtipe` DISABLE KEYS */;

INSERT INTO `mtipe` (`Uniq`, `Kategori`, `Tipe`, `Seq`)
VALUES
	(1,'LAPORAN','Pohon Tumbang',1),
	(2,'LAPORAN','Pengangkutan Sampah',2),
	(3,'LAPORAN','Lain-Lain',3),
	(4,'ADUAN','Dugaan Pencemaran Lingkungan',1),
	(5,'ADUAN','Supir Ugal-ugalan',2),
	(6,'ADUAN','Pungutan Liar',3),
	(7,'ADUAN','Lain-Lain',4);

/*!40000 ALTER TABLE `mtipe` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tlaporan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tlaporan`;

CREATE TABLE `tlaporan` (
  `LapID` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `LapKategori` varchar(100) NOT NULL DEFAULT '',
  `LapTipe` varchar(100) NOT NULL DEFAULT '',
  `LapJudul` varchar(100) NOT NULL DEFAULT '',
  `LapKeterangan` text,
  `LapStatus` enum('BARU','DIPROSES','SELESAI','DITOLAK') NOT NULL DEFAULT 'BARU',
  `LapFile` text,
  `LapPIC` text,
  `LapLokasi` text,
  `LapTanggal` date DEFAULT NULL,
  `CreatedBy` varchar(100) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`LapID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tlaporan` WRITE;
/*!40000 ALTER TABLE `tlaporan` DISABLE KEYS */;

INSERT INTO `tlaporan` (`LapID`, `LapKategori`, `LapTipe`, `LapJudul`, `LapKeterangan`, `LapStatus`, `LapFile`, `LapPIC`, `LapLokasi`, `LapTanggal`, `CreatedBy`, `CreatedOn`)
VALUES
	(1,'LAPORAN','Pohon Tumbang','Mohon Tindak Lanjut','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','DIPROSES',NULL,'198110032006041008',NULL,NULL,'085359867032','2022-10-31 22:09:47'),
	(2,'LAPORAN','Lain-Lain','Halo','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','BARU',NULL,NULL,NULL,NULL,'085359867032','2022-10-31 22:46:03'),
	(3,'LAPORAN','Lain-Lain','Test Laporan (2 Lampiran)','Testing','BARU','16648614424261.jpg,IMG-20221011-WA0032.jpg',NULL,NULL,NULL,'085359867032','2022-11-01 11:09:59'),
	(4,'LAPORAN','Lain-Lain','Lampiran Video','Testing','DIPROSES','IMG-20221011-WA0046.jpg,Sample_Videos___Dummy_Videos_For_Demo_Use.mp4','198209152005021002',NULL,NULL,'085359867032','2022-11-01 11:41:25'),
	(5,'LAPORAN','Pohon Tumbang','Test Lokasi dan Tanggal','Testing..','BARU',NULL,NULL,'Tao Toba','2022-10-20','085359867032','2022-11-01 23:58:47');

/*!40000 ALTER TABLE `tlaporan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tlaporanlog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tlaporanlog`;

CREATE TABLE `tlaporanlog` (
  `LogID` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `LapID` bigint(10) unsigned NOT NULL,
  `LogTipe` enum('STATUS','CATATAN') DEFAULT NULL,
  `LogRemarks` varchar(100) DEFAULT NULL,
  `LogFile` text,
  `LogUserName` varchar(100) DEFAULT '',
  `LogTimestamp` datetime NOT NULL,
  PRIMARY KEY (`LogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tlaporanlog` WRITE;
/*!40000 ALTER TABLE `tlaporanlog` DISABLE KEYS */;

INSERT INTO `tlaporanlog` (`LogID`, `LapID`, `LogTipe`, `LogRemarks`, `LogFile`, `LogUserName`, `LogTimestamp`)
VALUES
	(1,1,'STATUS','BARU',NULL,'SYSTEM','2022-10-31 22:09:47'),
	(2,1,'STATUS','DIPROSES',NULL,'197607312008011001','2022-10-31 22:12:32'),
	(3,2,'STATUS','BARU',NULL,'SYSTEM','2022-10-31 22:46:03'),
	(4,3,'STATUS','BARU',NULL,'SYSTEM','2022-11-01 11:09:59'),
	(5,4,'STATUS','BARU',NULL,'SYSTEM','2022-11-01 11:41:25'),
	(6,4,'STATUS','DIPROSES',NULL,'199410152019031007','2022-11-01 15:09:43'),
	(7,4,'CATATAN','Testing..','Sample_Videos___Dummy_Videos_For_Demo_Use1.mp4,SAPA.png','198209152005021002','2022-11-01 23:31:49'),
	(8,4,'CATATAN','Test..',NULL,'198209152005021002','2022-11-01 23:34:03'),
	(9,4,'CATATAN','Hello world..',NULL,'198209152005021002','2022-11-01 23:36:50'),
	(10,5,'STATUS','BARU',NULL,'SYSTEM','2022-11-01 23:58:47');

/*!40000 ALTER TABLE `tlaporanlog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpesan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpesan`;

CREATE TABLE `tpesan` (
  `PesanID` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `PesanDari` varchar(100) NOT NULL DEFAULT '',
  `PesanKe` varchar(100) DEFAULT NULL,
  `PesanText` text,
  `PesanFile` text,
  `PesanIsRead` tinyint(1) NOT NULL DEFAULT '0',
  `Timestamp` datetime NOT NULL,
  PRIMARY KEY (`PesanID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpesan` WRITE;
/*!40000 ALTER TABLE `tpesan` DISABLE KEYS */;

INSERT INTO `tpesan` (`PesanID`, `PesanDari`, `PesanKe`, `PesanText`, `PesanFile`, `PesanIsRead`, `Timestamp`)
VALUES
	(1,'085359867032',NULL,'Selamat malam.',NULL,1,'2022-10-31 21:10:14'),
	(2,'admin','085359867032','Selamat malam pak / bu. Perkenalkan saya Administrator, ada yang bisa dibantu?',NULL,1,'2022-10-31 21:11:42'),
	(9,'199410152019031007','085359867032','Halo pak / bu, ada kah yang bisa saya bantu untuk saat ini?',NULL,1,'2022-10-31 23:05:45');

/*!40000 ALTER TABLE `tpesan` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
